﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;
using System.Xml.Serialization;
using System.IO;


namespace TestApp.Controllers
{
    public class RelativesController : Controller
    {

        private prcs_testAppEntities db = new prcs_testAppEntities();

        //
        // GET: /Relatives/



        public ActionResult Index(string type="vis")
        {
            List<Relatives> arRelatives = db.Relatives.ToList();
            switch (type)
            {
                case "json":
                    try
                    {
                        
                        return Json(new { Result = "OK", Records = arRelatives }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                    };
                case "xml":
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(List<Relatives>));
                        using(var stream = new StringWriter())
                        {
                            ser.Serialize(stream, arRelatives);
                            return Content(stream.GetStringBuilder().ToString());
                        }
                        
                    }
                    catch(Exception ex)
                    {
                        List<string> list = new List<string>() { "ERROR",ex.Message };
                        XmlSerializer ser = new XmlSerializer(typeof(List<string>));
                        using (var stream = new StringWriter())
                        {
                            ser.Serialize(stream, list);
                            return Content(stream.GetStringBuilder().ToString());
                        }
                        
                    };
                default:
                var SearchSex = new SelectList(new[]
                                          {
                                              new{ID="",Name=""},
                                              new {ID="F",Name="Female"},
                                              new{ID="M",Name="Male"}                                             
                                          },
                                        "ID", "Name", "");
                ViewData["SearchSex"] = SearchSex;
                return View(arRelatives);
            }

        }

        //
        // GET: /Relatives/Details/5

        public ActionResult Details(int id = 0, string type="vis")
        {
            Relatives Relatives = db.Relatives.Find(id);
            switch (type)
            {
                case "json":                   
                    if (Relatives == null)
                    {
                        return Json(new { Result = "ERROR", Message = "Empty Set" }, JsonRequestBehavior.AllowGet); ;
                    }
                    try
                    {
                        return Json(new { Result = "OK", Records = Relatives }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                    };
                case "xml":
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Relatives));
                        using (var stream = new StringWriter())
                        {
                            ser.Serialize(stream, Relatives);
                            return Content(stream.GetStringBuilder().ToString());
                        }

                    }
                    catch (Exception ex)
                    {
                        List<string> list = new List<string>() { "ERROR", ex.Message };
                        XmlSerializer ser = new XmlSerializer(typeof(List<string>));
                        using (var stream = new StringWriter())
                        {
                            ser.Serialize(stream, list);
                            return Content(stream.GetStringBuilder().ToString());
                        }

                    };
                default:
                    return View(Relatives);
            }

        }

        //
        // GET: /Relatives/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Relatives/Create

        [HttpPost]
        public ActionResult Create(Relatives Relatives)
        {
            if (ModelState.IsValid)
            {
                db.Relatives.Add(Relatives);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Relatives);
        }

        //
        // GET: /Relatives/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Relatives Relatives = db.Relatives.Find(id);
            if (Relatives == null)
            {
                return HttpNotFound();
            }
            return View(Relatives);
        }

        //
        // POST: /Relatives/Edit/5

        [HttpPost]
        public ActionResult Edit(Relatives Relatives)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Relatives).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Relatives);
        }

        //
        // GET: /Relatives/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Relatives Relatives = db.Relatives.Find(id);
            if (Relatives == null)
            {
                return HttpNotFound();
            }
            return View(Relatives);
        }

        //
        // POST: /Relatives/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Relatives Relatives = db.Relatives.Find(id);
            db.Relatives.Remove(Relatives);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Search(string SearchSURNAME, string SearchNAME, string SearchLASTNAME, string SearchBDATE)
        {

            var Relatives = from m in db.Relatives
                         select m;

            if (!String.IsNullOrEmpty(SearchSURNAME))
            {
                Relatives = Relatives.Where(s => s.SurName.Contains(SearchSURNAME));
            }
            if (!String.IsNullOrEmpty(SearchNAME))
            {
                Relatives = Relatives.Where(s => s.Name.Contains(SearchNAME));
            }
            if (!String.IsNullOrEmpty(SearchLASTNAME))
            {
                Relatives = Relatives.Where(s => s.LastName.Contains(SearchLASTNAME));
            }
            if (!String.IsNullOrEmpty(SearchBDATE))
            {
                DateTime BDATE = new DateTime();
                BDATE = DateTime.ParseExact(SearchBDATE,"dd.MM.yyyy",System.Globalization.CultureInfo.InvariantCulture);// (SearchBDATE);
                Relatives = Relatives.Where(s => s.BirthDate==BDATE);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}